# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 18:25:34 2019

@author: Hououin Kyouma
"""
import numpy as np
import matplotlib.pyplot as plt
from functions import get_mat
from functions import lam_arr
from functions import psi_ana
from functions import rhs_easy_ana
from functions import get_psi_h
from functions import get_psi_num

nume = np.linspace(0,500,51,endpoint = True)
l = len(nume)
nume = nume.reshape(1,l)
nume[0,0] = 1
max_err = np.zeros((1,l))
th = 0
###Constants
i_vac = 0.3
a = 0.4841
R_0 = 10
N = 5
###Parameters
beta = 0.001
mu_0I_phi = 0
###Model Constants
A = -(4*R_0/N)*mu_0I_phi
del_2 = np.sqrt(i_vac/N)
c = (8*R_0* beta)/(a*N**2 *(A+2* del_2**2))

for k in range(1,l+1):
    n = k #number of intermediate values
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    
    ########Get Matrix
    M = get_mat(n)
    
    #######Get 'r' (The easy analytical case)
    r = rhs_easy_ana(c,n)
    psi_h = get_psi_h(M,r)  #Solve for Psi hat
    
    ##################COMPARISON########################
    rho_inp = np.linspace(0,1,num = 100,endpoint = True)
    psi_num_arr = np.zeros(len(rho_inp))
    psi_ana_arr = np.zeros(len(rho_inp))
    for i in range(len(rho_inp)):
        psi_num_arr[i] = get_psi_num(lam_arr(rho_inp[i],th,n),psi_h)
        psi_ana_arr[i] = psi_ana(rho_inp[i],th,c)   
    max_err[0,k-1] = np.amax(abs(psi_ana_arr -  psi_num_arr))
    
###########Plot
x = np.log10(nume)
y = np.log10(max_err)
x = np.ravel(x)
y = np.ravel(y)
z = np.polyfit(x[5:l], y[5:l], 1)
p = np.poly1d(z)
plt.figure(figsize=(10,10))
plt.plot(x,y,'ro',linewidth = 10)
plt.xlabel(r'$log_{10}(n)$')
plt.ylabel('$log_{10}(|Analytical\ Solution - Numerical\ solution|_{max}$ )') 
plt.plot(x, x*z[0] + z[1], 'b')
plt.legend(('Actual Convergence','Linear fit with slope = {}'.format(z[0])))
plt.title(r'$Convergence\/ Test\/ for\/ \psi_2(\rho,\theta)\/ where\/ \theta = 0 $')
plt.show()
