# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 18:32:24 2019

@author: Hououin Kyouma
"""

import numpy as np
from functions import get_mat
from functions import lam_arr
from functions import psi_ana
from functions import psi1_ana
from functions import rhs_easy_ana
from functions import get_psi_h
from functions import get_psi_num
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

n = 100 #number of finite elements
###Constants
i_vac = 0.3
a = 0.4841
R_0 = 10
N = 5
###Parameters
beta = 0.001
mu_0I_phi = 0
###Model Constants
A = -(4*R_0/N)*mu_0I_phi
del_2 = np.sqrt(i_vac/N)
c = (8*R_0* beta)/(a*N**2 *(A+2* del_2**2))

########Get Matrix
M = get_mat(n)

#######Get 'r' (The easy analytical case)
r = rhs_easy_ana(c,n)
psi_h = get_psi_h(M,r)  #Solve for Psi hat

#########Solution Setup
r = np.linspace(0,1,100,endpoint = True)
p = np.linspace(0,2*np.pi,100,endpoint = True)
R,P = np.meshgrid(r,p)
X,Y = R*np.cos(P),R*np.sin(P)

#########Psi numerical solution
psi_num = np.zeros((len(p),len(r)))
for i in range(len(p)):
    for j in range(len(r)):
        psi_num[i,j] = get_psi_num(lam_arr(r[j],p[i],n),psi_h)
        
########The analytical solution for comparison..
Z1 = psi1_ana(R,A,del_2)+psi_ana(R,P,c)
Z2 = psi1_ana(R,A,del_2)+psi_num

################## Polar plot #######################
fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(X, Y, Z1,cmap=cm.inferno)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title(r'$ Analytical \/ Solution\/ \psi(\rho,\theta) = \psi_1(\rho) + \psi_2(\rho,\theta) $')
plt.show()

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(X, Y, Z2,cmap=cm.inferno)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title(r'$ Numerical \/ Solution\/ \psi(\rho,\theta) = \psi_1(\rho) + \psi_2(\rho,\theta) $')
plt.show()

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(X, Y, (Z1-Z2),cmap=cm.inferno)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title('Absolute Error')
plt.show()