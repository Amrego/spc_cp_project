# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 15:20:45 2019

@author: Hououin Kyouma
"""
import numpy as np
import matplotlib.pyplot as plt
from functions import get_mat
from functions import lam_arr
from functions import psi_ana
from functions import rhs_easy_ana
from functions import get_psi_h
from functions import get_psi_num

n = 5 #number of intermediate values######
th = 0

###Constants
i_vac = 0.3
a = 0.4841
R_0 = 10
N = 5
###Parameters
beta = 0.001
mu_0I_phi = 0
###Model Constants
A = -(4*R_0/N)*mu_0I_phi
del_2 = np.sqrt(i_vac/N)
c = (8*R_0* beta)/(a*N**2 *(A+2* del_2**2))

########Get Matrix
M = get_mat(n)

#######Get 'r' (The easy analytical case)
r = rhs_easy_ana(c,n)
psi_h = get_psi_h(M,r)  #Solve for Psi hat
    
##################COMPARISON of psi_2########################
rho_inp = np.linspace(0,1,num = 100,endpoint = True)
psi_num_arr = np.zeros(len(rho_inp))
psi_ana_arr = np.zeros(len(rho_inp))
for i in range(len(rho_inp)):
    psi_num_arr[i] = get_psi_num(lam_arr(rho_inp[i],th,n),psi_h)
    psi_ana_arr[i] = psi_ana(rho_inp[i],th,c)

###########Plot
plt.figure(figsize=(10,10))
plt.plot(rho_inp,psi_num_arr,'ro',rho_inp,psi_ana_arr,'bx')
plt.xlabel(r'$\rho$')
plt.ylabel(r'$\psi_2 (\rho)$')
plt.title(r'Comparison of $\psi_2(\rho,\theta)\/ where\/ \theta = 0$')
plt.legend((r'Numerical  $\psi_2(\rho,0)$',r'Analytical $\psi_2(\rho,0)$'))
plt.show()