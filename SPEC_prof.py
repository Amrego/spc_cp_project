# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 10:45:35 2019

@author: Hououin Kyouma
"""
import numpy as np
from functions import get_mat
from functions import lam_arr
from functions import get_psi_num
from functions import get_G
from functions import GQ_int_spec
from functions import i_psi_c
from functions import psi1_spec
from gq_coeffs import GQ_cw
from functions import del_x
import matplotlib.pyplot as plt
from matplotlib import cm


cst = 0.2

n = 300 #number of intermediate values
ngq_r = 9
ngq_t = 9
###Constants
i_vac = 0.3
a = 0.4841
R_0 = 10
N = 5
###Parameters
beta = 0.02
mu_0I_phi = 0.01
###Model Constants
A = -(4*R_0/N)*mu_0I_phi
del_2 = np.sqrt(i_vac/N)
c = (8*R_0* beta)/(a*N**2 *(A+2* del_2**2))
###Newton Method Parameters
delta = 1e-10
tol = 1e-14
max_iter = 1000
#psh_gs = np.load('psi_h_guess.npy')
#psh = 100*psh_gs
#psh[-1,0] = 0
psh = np.zeros((n,1))
########Get Matrix
M = get_mat(n)


err = np.zeros((1,max_iter))

for it in range(max_iter):
    i_psi = i_psi_c(A,del_2,psh[-1,-1],n)
    #######Get 'r' (by gauss quad)
    cw_r = GQ_cw(ngq_r)
    cw_t = GQ_cw(ngq_t)
    r_vec = GQ_int_spec(cw_r,cw_t,c,n,i_psi)
    
    #get G
    G_0 = get_G(M,psh,r_vec)
    
    #get jacob g
    J_G = np.zeros((len(r_vec),len(psh)))
    for j in range(len(psh)):
        Gp = get_G(M,psh + del_x(psh,j,delta),r_vec)
        Gm = get_G(M,psh - del_x(psh,j,delta),r_vec)
        J_G[:,j]= np.ravel((0.5/delta)*(Gp - Gm)) 
        
    #check jacob, if det 0 then exit
    if np.linalg.det(J_G) == 0:
        print('det (jacob) is 0')
        break
    
    #update psi h
    psh_tmp = psh - np.linalg.inv(J_G) @ G_0
    psh_new = cst * psh + (1 - cst) * psh_tmp
    
    i_psi_new = i_psi_c(A,del_2,psh_new[-1,-1],n)
    r_vec_new = GQ_int_spec(cw_r,cw_t,c,n,i_psi_new)
    
    
    
    
    #get new g
    G_new = get_G(M,psh_new,r_vec_new)
    
    new_error = np.amax(abs(G_new))
    
    
    #check new g
    if new_error > tol:
        err[0,it] = new_error
        psh = psh_new
    else:
        print('Solution is here..')
        break
  
#########Solution Setup
r = np.linspace(0,1,100,endpoint = True)
p = np.linspace(0,2*np.pi,100,endpoint = True)
R,P = np.meshgrid(r,p)
X,Y = R*np.cos(P),R*np.sin(P)

#########Psi numerical solution
psi_num = np.zeros((len(p),len(r)))
for i in range(len(p)):
    for j in range(len(r)):
        psi_num[i,j] = get_psi_num(lam_arr(r[j],p[i],n),psh_new)

Z2 = psi1_spec(R,A,del_2) + psi_num
fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(X, Y, Z2,cmap=cm.inferno)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title('Numerical Solution')
plt.show()