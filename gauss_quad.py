# -*- coding: utf-8 -*-
"""
Created on Fri Nov 15 15:44:04 2019

@author: Hououin Kyouma
"""
import numpy as np
from functions import get_mat
from functions import lam_arr
from functions import psi_ana
from functions import psi1_ana
from functions import get_psi_h
from functions import get_psi_num
from functions import rhs_easy_ana
from functions import GQ_int
from gq_coeffs import GQ_cw
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

n = 300 #number of intermediate values
ngq_r = 9
ngq_t = 9
###Constants
i_vac = 0.3
a = 0.4841
R_0 = 10
N = 5
###Parameters
beta = 0.001
mu_0I_phi = 0
###Model Constants
A = -(4*R_0/N)*mu_0I_phi
del_2 = np.sqrt(i_vac/N)
c = (8*R_0* beta)/(a*N**2 *(A+2* del_2**2))

########Get Matrix
M = get_mat(n)

#######Get 'r' (by gauss quad)
cw_r = GQ_cw(ngq_r)
cw_t = GQ_cw(ngq_t)
r_vec = GQ_int(cw_r,cw_t,c,n)
r_bagha = rhs_easy_ana(c,n)

######Solve for Psi hat
psi_h = get_psi_h(M,r_vec) 
np.save('psi_h_guess.npy',psi_h) 

#########Solution Setup
r = np.linspace(0,1,100,endpoint = True)
p = np.linspace(0,2*np.pi,100,endpoint = True)
R,P = np.meshgrid(r,p)
X,Y = R*np.cos(P),R*np.sin(P)

#########Psi numerical solution
psi_num = np.zeros((len(p),len(r)))
for i in range(len(p)):
    for j in range(len(r)):
        psi_num[i,j] = get_psi_num(lam_arr(r[j],p[i],n),psi_h)
        
########The analytical solution for comparison..
Z1 = psi1_ana(R,A,del_2)+psi_ana(R,P,c)
Z2 = psi1_ana(R,A,del_2)+psi_num

################## Polar plots #######################
fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(X, Y, Z1,cmap=cm.inferno)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title('Analytical Solution')
plt.show()

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(X, Y, Z2,cmap=cm.inferno)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title('Numerical Solution')
plt.show()

plt.figure(figsize=(10,10))
#plt.plot(r,Z2[0,:],'ro',r,Z1[0,:],'bx')
plt.plot(r,Z2[0,:]-Z1[0,:])
plt.xlabel(r'$\rho$')
plt.ylabel('$\psi$') 
plt.title('Comparison of Solutions')
plt.legend(('Numerical $\psi$','Analytical $\psi$'))
plt.show()