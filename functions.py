# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 19:05:25 2019

@author: Hououin Kyouma
"""

import numpy as np
import sys

########Get Matrix
def get_mat(ne):
    rho = np.linspace(0,1,num = ne+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    M = np.zeros((ne,ne))
    for i in range(1,ne+1):
        for j in range(1,ne+1):
            if j == i-1:
                M[i-1,j-1] = (np.pi/(d_rho**2))*((rho[j]*rho[i]*np.log(rho[i]/rho[j])))
            elif j == i:
                if i == 1:
                    M[i-1,j-1] = (np.pi/d_rho**2)*((-rho[i+1]**2)*np.log(rho[i+1]/rho[i]))
                else:
                    M[i-1,j-1] = (np.pi/d_rho**2)*((-rho[i-1]**2)*np.log(rho[i]/rho[i-1])-(rho[i+1]**2)*np.log(rho[i+1]/rho[i]))
            elif j == i+1:
                M[i-1,j-1] = (np.pi/(d_rho**2))*((rho[j]*rho[i]*np.log(rho[j]/rho[i])))
            else:
                M[i-1,j-1] = 0
    Mat = np.transpose(M)
    return Mat

########Finite Element decomposition: Lambda(rho) 
def lami(ind,rho_inp,n):
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    if rho_inp <= rho[ind-1]:
        return 0; 
    elif rho_inp>rho[ind-1] and rho_inp<rho[ind]:
        rho_out1 = (rho_inp - rho[ind-1])/d_rho
        return rho_out1;
    elif rho_inp>rho[ind] and rho_inp<rho[ind+1]:
        rho_out2 = (rho[ind+1] - rho_inp)/d_rho
        return rho_out2; 
    elif rho_inp == rho[ind]:
        return 1; 
    elif rho_inp >= rho[ind+1]:
        return 0;
    
########Array of Lambda
def lam_arr(rho_inp,th,n):
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    lam_arr = np.zeros((1,n))
    def lam(ind,rho_inp):
        if rho_inp <= rho[ind-1]:
            return 0; 
        elif rho_inp>rho[ind-1] and rho_inp<rho[ind]:
            rho_out1 = (rho_inp - rho[ind-1])/d_rho
            return rho_out1;
        elif rho_inp>rho[ind] and rho_inp<rho[ind+1]:
            rho_out2 = (rho[ind+1] - rho_inp)/d_rho
            return rho_out2; 
        elif rho_inp == rho[ind]:
            return 1; 
        elif rho_inp >= rho[ind+1]:
            return 0;
    for i in range(1,n+1):
        lam_arr[0,i-1] = lam(i,rho_inp)*np.cos(th)
    return lam_arr


def rhs_easy_ana(c,n):
    r = np.zeros((n,1))
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    for i in range(1,n+1):
        r[i-1,0] = c*(np.pi/d_rho)*((rho[i]**4)/6 -(rho[i-1]**4 + rho[i+1]**4)/12)
    return r

def get_psi_h(M,r):
    M_inv = np.linalg.inv(M)
    psi_h = M_inv @ r
    return psi_h

def get_psi_num(lam_arr,psi_h):
    psi_num = np.sum(lam_arr * np.transpose(psi_h))
    return psi_num

#######Easy RHS function
def arb_fun(c,r,th):
    val = -c * r * np.cos(th)
    return val

######The Easy analytical solution 
def psi_ana(rho_inp,th,c):
    psi = (c/8)*(1-rho_inp**2)*rho_inp*np.cos(th) #+ 50*(1-rho_inp**2)
    return psi

def psi1_ana(rho_inp,A,del_2):
    psi1 = (1-rho_inp**2)*(A/4 + del_2**2 / 2)
    return psi1

def psi1_spec(rho_inp,A,del_2):
    psi1 = (4*A/25)*(1-rho_inp**(5/2)) +(1-rho_inp**2)* del_2**2 / 2
    return psi1

####GQ Integration
def GQ_int(cw_r,cw_t,c,n):
    r_vec = np.zeros((n,1))
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    rh_pf= 0.5 * (cw_r[0,:] + 1)*(d_rho)
    th = np.pi * (cw_t[0,:] + 1)
    int = np.zeros((len(th),len(rh_pf)))
    for i in range(1,n+1):
        for j in range(len(rh_pf)):
            for k in range(len(th)):
                int[k,j] = np.pi*(d_rho/2)*cw_t[1,k]*cw_r[1,j]*arb_fun(c,rh_pf[j]+rho[i-1],th[k])*(rh_pf[j]/d_rho)*np.cos(th[k])*(rh_pf[j]+rho[i-1]) + np.pi*(d_rho/2)*cw_t[1,k]*cw_r[1,j]*arb_fun(c,rh_pf[j]+rho[i],th[k])*((d_rho - rh_pf[j])/d_rho)*np.cos(th[k])*(rh_pf[j]+rho[i])           
        r_vec[i-1,0] = int.sum()
    return r_vec

def i_psi_c(A,del_2,psin_h,n):
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    try:
        i = 5*np.sqrt((2*A/5 + del_2**2)**2 - (psin_h/d_rho)**2)
    except:
        print("Error in i_psi_c:", sys.exc_info()[0])
        i = 1E99
    return i

#####rhs function for spec
    
def GQ_int_spec(cw_r,cw_t,c,n,i):
    def arb_fun_spec(c,r,th,i):
        val = -c * r * np.cos(th)/i
        return val
    r_vec = np.zeros((n,1))
    rho = np.linspace(0,1,num = n+2,endpoint = True)
    d_rho = rho[1]-rho[0]
    rh_pf= 0.5 * (cw_r[0,:] + 1)*(d_rho)
    th = np.pi * (cw_t[0,:] + 1)
    int = np.zeros((len(th),len(rh_pf)))
    for i in range(1,n+1):
        for j in range(len(rh_pf)):
            for k in range(len(th)):
                int[k,j] = np.pi*(d_rho/2)*cw_t[1,k]*cw_r[1,j]*arb_fun_spec(c,rh_pf[j]+rho[i-1],th[k],i)*(rh_pf[j]/d_rho)*np.cos(th[k])*(rh_pf[j]+rho[i-1]) + np.pi*(d_rho/2)*cw_t[1,k]*cw_r[1,j]*arb_fun(c,rh_pf[j]+rho[i],th[k])*((d_rho - rh_pf[j])/d_rho)*np.cos(th[k])*(rh_pf[j]+rho[i])           
        r_vec[i-1,0] = int.sum()
    return r_vec

def get_G(M,psh,r):
    G = M @ psh - r
    return G

def del_x(psh,j,delta):
    del_x = np.zeros((len(psh),1))
    del_x[j,0] = delta
    return del_x

    
        

    
    
    
    
    
    
    
    
    
        